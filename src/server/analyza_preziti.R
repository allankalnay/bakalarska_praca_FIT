output$krivka_preziti_input_panel <- renderUI(
  wellPanel(
    sliderInput("krivka_preziti_input_years", 'Roky:', 
                min = unique_transplant_years_decreasing[length(unique_transplant_years_decreasing)],
                max = unique_transplant_years_decreasing[1],
                value = c(unique_transplant_years_decreasing[length(unique_transplant_years_decreasing)],
                          unique_transplant_years_decreasing[1]),
                step = 1,
                sep = ''),
    numericInput('krivka_preziti_input_seskupit_po', 'Seskupit po:',
                 value = 5,
                 min = 1),
    selectInput('krivka_preziti_input_diagnosticka_skupina', 'Diagnostická skupina:', 
                choices = c('Všechny', unique_dg_group_sorted)),
    selectInput('krivka_preziti_input_hctci_pasmo', 'HCTCI pásmo:', 
                choices = c(input_nothing_selected, hctci_pasma_names)),
    checkboxInput('krivka_preziti_input_facet', 'Facet', value = FALSE),
    width = 12
  )
)

output$krivka_preziti <- renderPlot(
  {
    krivka_year_bottom <- input$krivka_preziti_input_years[1]
    krivka_year_top <- input$krivka_preziti_input_years[2]
    krivka_dg_group <- input$krivka_preziti_input_diagnosticka_skupina
    krivka_hctci_pasmo <- input$krivka_preziti_input_hctci_pasmo
    krivka_seskupit_po <- input$krivka_preziti_input_seskupit_po
    krivka_facet <- input$krivka_preziti_input_facet
    
    if(any(
      sapply(list(krivka_year_bottom, 
                  krivka_year_top, 
                  krivka_dg_group, 
                  krivka_hctci_pasmo,
                  krivka_seskupit_po,
                  krivka_facet), is.null)
    ) == TRUE)
      return()
    
    years_seq <- seq(krivka_year_bottom, krivka_year_top)
    # data which fit the range of selected years
    # data which meet the condition that doba_preziti is not NA
    data_krivka_preziti <- data_tab1 %>% 
      select(doba_preziti, pacient_zemrel, hctci_score, dg_group, rok_transplantace) %>%
      filter(rok_transplantace %in% years_seq & !is.na(doba_preziti) & doba_preziti >= 0) %>%
      as.data.frame()
    
    data_krivka_preziti$hctci_pasmo <- get_hctci_pasma_from_df(data_krivka_preziti)
      
    # filter dg_group by user input
    if(krivka_dg_group != input_nothing_selected) {
      data_krivka_preziti <- data_krivka_preziti %>%
        filter(dg_group == krivka_dg_group)
    }
    
    # filter hctci_pasmo by user input
    if(krivka_hctci_pasmo != input_nothing_selected) {
      data_krivka_preziti <- data_krivka_preziti %>% 
        filter(hctci_pasmo == krivka_hctci_pasmo)
    }
    
    
    # if seskupit_po != 1, then cut
    if(krivka_seskupit_po != 1) {
      intervals_info <- create_data_for_setting_time_period(krivka_year_bottom,
                                                            krivka_year_top,
                                                            krivka_seskupit_po)
      data_krivka_preziti$casove_obdobi <- cut(as.numeric(data_krivka_preziti$rok_transplantace),
                                               breaks = intervals_info$years_sequence,
                                               labels = intervals_info$labels,
                                               include.lowest = T)
      data_krivka_preziti <- data_krivka_preziti[!is.na(data_krivka_preziti$casove_obdobi),]
      #data_krivka_preziti$casove_obdobi <- as.factor(data_krivka_preziti$casove_obdobi)
    }
    else {
      data_krivka_preziti$casove_obdobi <- data_krivka_preziti$rok_transplantace
    }
    
    # validate number of rows of data set > 0
    shiny::validate(
      need(nrow(data_krivka_preziti) > 0, announcement_narrow_input)
    )
    
    krivka_preziti_xlim_upper_bound <- (sort(data_krivka_preziti$doba_preziti, decreasing = T)[1]/365) + 1
    
    surv_obj <- do.call(Surv, list(data_krivka_preziti$doba_preziti/365,data_krivka_preziti$pacient_zemrel))
    fit <- do.call(survfit, list(surv_obj ~ casove_obdobi, data = data_krivka_preziti))
    
    if(krivka_facet == FALSE) {
      krivka_preziti_plt <- ggsurvplot(fit,
                                       linetype = c('solid'),
                                       ggtheme = theme_bw(),
                                       surv.scale = 'percent',
                                       xlab = 'Roky',
                                       ylab = '%',
                                       censor = FALSE,
                                       xlim = c(0, krivka_preziti_xlim_upper_bound),
                                       break.x.by = 1,
                                       break.y.by = 0.1)
      plot2 <- krivka_preziti_plt +
        geom_dl(aes(label = casove_obdobi), 
                method = list("last.points"), 
                cex = 0.8)
    }
    else {
      "ggsurvplot(fit, 
      linetype = c('solid'),
      ggtheme = theme_bw(),
      surv.scale = 'percent',
      xlab = 'Roky',
      ylab = '%',
      censor = FALSE,
      break.x.by = 1,
      break.y.by = 0.1,
      facet.by = c('casove_obdobi'))"
      plot2 <- autoplot(fit,
                        facets = TRUE,
                        ncol = 5,
                        conf.int = FALSE,
                        censor = FALSE) +
        scale_x_continuous(breaks = seq(0, 100, 5))
    }
    
    plot2
    
    #surv_obj <- Surv(data_krivka_preziti$doba_preziti/365,data_krivka_preziti$pacient_zemrel)
    #fit <- survfit(surv_obj ~ casove_obdobi, data = data_krivka_preziti)
    #s <- npsurv(surv_obj ~ casove_obdobi, data = data_krivka_preziti)
    #survplot(s, col = 1:50)
    #autoplot(fit, facets = TRUE, ncol = 5)
    #ggsurvplot(fit)
    #ggsurvplot(fit)
  }
  )