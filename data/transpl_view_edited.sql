USE `shaker`;
CREATE  OR REPLACE VIEW `transpl` AS
SELECT `transplantace`.`id`,
    `transplantace`.`cislo_protokolu`,
    `transplantace`.`jmn`,
    `transplantace`.`rc`,
    `transplantace`.`por_tx`,
    `transplantace`.`vek`,
     IF(MID(rc, 3,2) > 50, "�ena", "mu�") as pohlavi,
    `transplantace`.`dg_group`,
    `transplantace`.`dg_dat`,
    `transplantace`.`tx_dat`,
    `hctci`.hctci_score,
    `transplantace`.`doba_preziti`,
    `transplantace`.`pacient_zemrel`,
    `transplantace`.`exitus` as datum_umrti,
     (select r.datum_relapsu from relaps as r where typ_relapsu = 2 and r.pac_id = transplantace.id order by r.datum_relapsu asc limit 1) as "datum_hemat_relapsu",
     (select datediff(datum_relapsu,tx_dat) from relaps as r where typ_relapsu = 2 and r.pac_id = transplantace.id order by r.datum_relapsu asc limit 1) as "doba_do_relapsu",
     
     IFNULL(
            (select 1 from relaps as r where typ_relapsu = 2 and r.pac_id = transplantace.id order by r.datum_relapsu asc limit 1), 0
      ) as "relaps",

     if (transplantace.pacient_zemrel,
         IFNULL(
            (select 0 from relaps as r where typ_relapsu = 2 and r.pac_id = transplantace.id order by r.datum_relapsu asc limit 1), 1
        ), 0
     ) as "NRM",
   
    if ((select 1 from relaps as r where typ_relapsu = 2 and r.pac_id = transplantace.id order by r.datum_relapsu asc limit 1),
            (select datediff(datum_relapsu,tx_dat) from relaps as r where typ_relapsu = 2 and r.pac_id = transplantace.id order by r.datum_relapsu asc limit 1),
            doba_preziti
    ) as DFS,
           
    `transplantace`.`last_fup`,
    `transplantace`.`last_fup_type`,
    (select max(validovano_do) from validace where validace.pac_id = transplantace.id) as validace,

    den100.aGVHD_grade,
    den100.dat_aGVHD,
        den100.aGVHD_horni_git,
        den100.aGVHD_dolni_git,
        den100.aGVHD_kuze,
        den100.aGVHD_jatra,
    den100.komplet as den100_komplet,
 
     least(
               IFNULL(min(followup.datum_hodnoceni), min(cgvhd.datum)), 
               IFNULL(min(cgvhd.datum), min(followup.datum_hodnoceni))
             ) as prvni_datum_cGVHD,

     greatest(
              IFNULL(max(followup.cGVHD_grade), max(cgvhd.cGVHD_grade)),
              IFNULL(max(cgvhd.cGVHD_grade), max(followup.cGVHD_grade))
            ) as  max_cGVHD_grade,

    `transplantace`.`pripravny_rezim`,
    `transplantace`.`GvHD_profylaxe`,
    `transplantace`.`rezim`,
    `transplantace`.`source` as typ_stepu,
    `transplantace`.`typ_darce`,
    `transplantace`.`darce`,
    `transplantace`.`HLA`,
    `transplantace`.`HLA_kod`,
    `transplantace`.`vek_darce`,
    `transplantace`.`pohlavi_darce`,
    `transplantace`.`id_darce`,
    `transplantace`.`KS_pac`,
    `transplantace`.`KS_darce`,
    `transplantace`.`CMV_pac`,
    `transplantace`.`CMV_darce`,
    `transplantace`.`EBV_pac`,
    `transplantace`.`EBV_darce`,
    `transplantace`.`Karnoffsky`,
    `transplantace`.`hmotnost`,
    `transplantace`.`vyska`,
    `transplantace`.`atg`,
    `transplantace`.`ebmt_score`,
    `transplantace`.`datum_propusteni`,
    `transplantace`.`komplet`,
    `transplantace`.`rci_darce`,
    `transplantace`.`datroz_darce`,
    `transplantace`.`stat`,
    `transplantace`.`okres`,
    `transplantace`.`poj`,
    `transplantace`.`typ_predchozi_hct`,
    `transplantace`.`dat_predchozi_hct`,
    `transplantace`.`multigraft_protocol`,
    EXTRACT(YEAR FROM transplantace.tx_dat) as rok_transplantace

FROM `shaker`.`transplantace`

    LEFT JOIN den100 on transplantace.id = den100.pac_id

    LEFT JOIN
        followup ON transplantace.ID = followup.pac_id and followup.cGVHD_grade > 0
    LEFT JOIN
        cgvhd ON transplantace.ID = cgvhd.pac_id 
    LEFT JOIN
        hctci ON transplantace.ID = hctci.id 

group by transplantace.id
order by transplantace.id desc

;
